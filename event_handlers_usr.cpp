/**
 * This file contains all functions (handlers) that are needed by generated gui
 * This functions must be provided for linking
 */

#include "windows_usr.hpp"
#include <cstdio>
#include <random>

using namespace ct_gui;
using namespace translate;

void handle_SOME_BUTTON(void *param) {
  if (windows[windowId::WINDOW_1].widgets[widgetId::RANDOM_NUMBER].visible ==
      widget::visibility::HIDDEN) {
    windows[windowId::WINDOW_1].widgets[widgetId::RANDOM_NUMBER].visible =
        widget::visibility::VISIBLE;
  } else {
    windows[windowId::WINDOW_1].widgets[widgetId::RANDOM_NUMBER].visible =
        widget::visibility::HIDDEN;
  }
}
void handle_LANGUAGE_BUTTON(void *param) {
  setLanguage(static_cast<language>(rand() % 4));
}

// simple event handler getting random data to prove concept
void usr_value_event(int value, char buff[20]) {
  switch (static_cast<value_get_event>(value)) {
  case value_get_event::RANDOM:
    snprintf(buff, 19, "%d", rand());
    break;
  case value_get_event::ADDRESS:
    snprintf(buff, 19, "%d.%d.%d.%d", rand() % 255, rand() % 255, rand() % 255,
             rand() % 255);
    break;
  default:
    break;
  }
}