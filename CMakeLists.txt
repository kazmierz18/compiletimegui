cmake_minimum_required(VERSION 3.0.0)
project(compileTimeGUI VERSION 0.1.0)

set(CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -O0 -g3 -Wpedantic")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -O2")

# include(CTest)
# enable_testing()

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
include_directories( ${OPENGL_INCLUDE_DIRS}  ${GLUT_INCLUDE_DIRS} )

include_directories(.)

add_subdirectory(driver)
# add_subdirectory(fmt)

add_subdirectory(translations)
add_subdirectory(ct_gui)

add_executable(compileTimeGUI main.cpp event_handlers_usr.cpp)
target_link_libraries(compileTimeGUI ct_gui ${OPENGL_LIBRARIES} ${GLUT_LIBRARY})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
# include(CPack)
