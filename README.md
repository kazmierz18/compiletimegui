# This is simple GUI framework that is created in compile time

It's purpose is small embedded systems with primitive displays
This library is mainly proof of concept, for learning purposes.

Goals:
* Screens are generated during compile time ( no need to create windows in runtime on initialization)
* Small footprint
* Basics widgets implementations
* Use only c parser for creating most of the automatic stuff

Restrictions:
* Keep allocation usage as less as possible