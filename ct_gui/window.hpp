#pragma once
#include "cstdint"
#include "widget.hpp"

namespace ct_gui {
const std::size_t MAX_WIDGETS = 10;
struct window {
  const char *name;
  widget widgets[10];
  widget *focused_widget;
  void draw();
  bool handle_event(event &e);
};

} // namespace ct_gui
