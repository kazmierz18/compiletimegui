#pragma once
#include "renderer.hpp"
#include "windows_usr.hpp"

namespace ct_gui {

void renderer_init(windowId id);
void renderer_start();

typedef void (*value_get_fn_t)(int, char[20]);
void registerValueGet(value_get_fn_t f);

} // namespace ct_gui