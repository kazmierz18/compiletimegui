#pragma once

namespace ct_gui {

struct menu_item {
  const char *debug_name;
  int sub_menu;
  int text;
};

struct menu {
  const char *debug_name;
  menu_item items[20];
};

} // namespace ct_gui