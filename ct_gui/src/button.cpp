#include "driver.hpp"
#include "translations.hpp"
#include "widget.hpp"

using namespace translate;
namespace ct_gui {

bool widget::handle_event(event &e) {
  if (visible == widget::visibility::VISIBLE) {
    if (e.x > x and e.x < x + width and e.y > y and e.y < y + height) {
      if (e.t == event::type::CLICK_DOWN) {
        focused = true;
      } else if (e.t == event::type::CLICK_UP) {
        focused = false;
        state = !state;
        if (fn) {
          fn(nullptr);
        }
      }
      driver::redraw();
      return true;
    }
  }
  if (focused) {
    focused = false;
    driver::redraw();
  }

  return false;
}

void widget::draw_button() {
  int color = focused ? 0 : 1;
  driver::draw_rect(x, y, width, height, color);

  driver::draw_text(
      getTranslation(static_cast<textId>(data_src), getLanguage()), x + 3,
      y + (height / 2));
}

} // namespace ct_gui
