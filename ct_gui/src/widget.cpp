#include "driver.hpp"
#include "translations.hpp"

#include "renderer.hpp"
#include "widget.hpp"

namespace ct_gui {

using namespace translate;

extern value_get_fn_t value_event;

void widget::draw() {
  if (visible != visibility::HIDDEN) {
    int color = focused ? 0 : 1;
    switch (type) {
    case type::BUTTON:
      draw_button();
      break;
    case type::LABEL:
      if (static_cast<textId>(data_src) == textId::_TXT_NO_TEXT) {
        if (value_event) {
          char buff[20] = {0};
          value_event(value_get_event, buff);
          driver::draw_text(buff, x + 3, y + (height / 2));
        }
      } else {
        driver::draw_text(
            getTranslation(static_cast<textId>(data_src), getLanguage()), x + 3,
            y + (height / 2));
      }
      break;
    case type::IMAGE:
      driver::draw_text("image", x + 3, y + (height / 2));
      break;
    case type::MENU_CONTAINER:
      driver::draw_rect(x, y, width, height, color);
      break;
    case type::CHECK_BOX:
      break;
    case type::COMBO_BOX:
      break;
    case type::EMPTY:
      break;
    }
  }
}

} // namespace ct_gui