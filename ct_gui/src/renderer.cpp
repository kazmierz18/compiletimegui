#include "renderer.hpp"
#include "driver.hpp"
#include <GL/glut.h>

namespace ct_gui {

/**
 * This is currently displayed window
 */
window *current_window;

void renderer() {
  glClear(GL_COLOR_BUFFER_BIT);
  current_window->draw();
  glFlush();
}

void click_handle(int button, int state, int x, int y) {
  event e = {x, y, state ? event::type::CLICK_UP : event::type::CLICK_DOWN};
  current_window->handle_event(e);
}

void renderer_init(windowId id) {
  int argc = 0;
  char **argv = nullptr;
  current_window = &windows[id];
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(driver::windows_width, driver::windows_height);
  glutCreateWindow("Hello World");
  glutMouseFunc(click_handle);
  glutDisplayFunc(renderer);
}
void renderer_start() { glutMainLoop(); }

value_get_fn_t value_event;
void registerValueGet(value_get_fn_t f);

typedef void (*value_get_fn_t)(int, char[20]);
void registerValueGet(value_get_fn_t f) { value_event = f; }

} // namespace ct_gui
