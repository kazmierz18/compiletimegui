#include "window.hpp"
#include "widget.hpp"

namespace ct_gui {

void window::draw() {
  for (std::size_t i = 0; i < MAX_WIDGETS; i++) {
    if (widgets[i].type != widget::type::EMPTY) {
      widgets[i].draw();
    }
  }
}

bool window::handle_event(event &e) {
  if (focused_widget) {
    if (focused_widget->handle_event(e)) {
      return true;
    }
  }
  for (std::size_t i = 0; i < MAX_WIDGETS; i++) {
    if (widgets[i].type != widget::type::EMPTY) {
      if (widgets[i].handle_event(e)) {
        focused_widget = &widgets[i];
        return true;
      }
    }
  }
  focused_widget = nullptr;
  return false;
}
} // namespace ct_gui