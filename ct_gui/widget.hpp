#pragma once

namespace ct_gui {

struct event {
  int x;
  int y;

  enum class type {
    CLICK_DOWN,
    CLICK_UP,
  };
  type t;
};

struct widget {
  enum class type {
    EMPTY = 0, // must be 0
    LABEL,
    IMAGE,
    BUTTON,
    MENU_CONTAINER,
    CHECK_BOX,
    COMBO_BOX,
  };
  enum class visibility {
    VISIBLE = 0, // must be 0
    DISABLED,
    HIDDEN,
  };
  // only for debug
  const char *debug_name;

  // coordinates
  int x;
  int y;
  int width;
  int height;

  const type type;
  visibility visible;

  int data_src;
  enum class alignment {
    LEFT,
    CENTER,
    RIGHT,
  };
  alignment align;
  int value_get_event;
  typedef void (*fn_handle_t)(void *);
  fn_handle_t fn;

  // runtime state of widget, initialized to 0

  bool focused;
  bool state;

  /**
   * Draws widget on screen using driver
   */
  void draw();

  /**
   * Handles input events
   * @param e: input event from driver
   * @return true if event handled, otherwise false
   */
  bool handle_event(event &e);

  void draw_button();
};

} // namespace ct_gui