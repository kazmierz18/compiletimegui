#pragma once

#include "translations.hpp"
#include "widget.hpp"
#include "window.hpp"

#define WINDOWS_USR_FILE "../usr_gen/windows.usr"

/**
 * Content of this file is created during preprocessing
 * If you really want to dig in to this check the preprocessor output
 */

/**
 * This are events generated during redraw for dynamic texts
 */
#define CREATE_WINDOW(name)
#define ADD_LABEL(name, x, y, width, height, visibility, textId, align)
#define ADD_IMAGE(name, x, y, width, height, visibility, imageId)
#define ADD_BUTTON(name, x, y, width, height, visibility, textId, align)
#define ADD_VALUE_LABEL(name, x, y, width, height, visibility, source, align)  \
  source,
#define ADD_MENU_CONTAINER(name, x, y, width, height, visibility, textId, align)
#define END_WINDOW()

enum value_get_event {
#include WINDOWS_USR_FILE
};

#undef CREATE_WINDOW
#undef ADD_LABEL
#undef ADD_IMAGE
#undef ADD_BUTTON
#undef ADD_VALUE_LABEL
#undef ADD_MENU_CONTAINER
#undef END_WINDOW

/**
 * Create enum for each window to index it in windows array
 */
#define CREATE_WINDOW(name) name,
#define ADD_LABEL(name, x, y, width, height, visibility, textId, align)
#define ADD_IMAGE(name, x, y, width, height, visibility, imageId)
#define ADD_BUTTON(name, x, y, width, height, visibility, textId, align)
#define ADD_VALUE_LABEL(name, x, y, width, height, visibility, source, align)
#define ADD_MENU_CONTAINER(name, x, y, width, height, visibility, textId, align)
#define END_WINDOW()

enum windowId {
#include WINDOWS_USR_FILE
};

#undef CREATE_WINDOW
#undef ADD_LABEL
#undef ADD_IMAGE
#undef ADD_BUTTON
#undef ADD_VALUE_LABEL
#undef ADD_MENU_CONTAINER
#undef END_WINDOW

/**
 * Create enum for each widget in window to index it in windows array
 *
 * Be careful that there are created enums with _ at the begining which
 * are there only for reseting numeration for each new window
 *  Attention! this may not work on all compilers
 */
#define CREATE_WINDOW(name) _##name = -1,
#define ADD_LABEL(name, x, y, width, height, visibility, textId, align) name,
#define ADD_IMAGE(name, x, y, width, height, visibility, imageId) name,
#define ADD_BUTTON(name, x, y, width, height, visibility, textId, align) name,
#define ADD_VALUE_LABEL(name, x, y, width, height, visibility, source, align)  \
  name,
#define ADD_MENU_CONTAINER(name, x, y, width, height, visibility, textId, align)
#define END_WINDOW()

enum widgetId {
#include WINDOWS_USR_FILE
};

#undef CREATE_WINDOW
#undef ADD_LABEL
#undef ADD_IMAGE
#undef ADD_BUTTON
#undef ADD_VALUE_LABEL
#undef ADD_MENU_CONTAINER
#undef END_WINDOW

/**
 * Create declarations of handlers for buttons
 */
#define CREATE_WINDOW(name)
#define ADD_LABEL(name, x, y, width, height, visibility, textId, align)
#define ADD_IMAGE(name, x, y, width, height, visibility, imageId)
#define ADD_BUTTON(name, x, y, width, height, visibility, textId, align)       \
  void handle_##name(void *);
#define ADD_VALUE_LABEL(name, x, y, width, height, visibility, source, align)
#define ADD_MENU_CONTAINER(name, x, y, width, height, visibility, textId, align)
#define END_WINDOW()

#include WINDOWS_USR_FILE

#undef CREATE_WINDOW
#undef ADD_LABEL
#undef ADD_IMAGE
#undef ADD_BUTTON
#undef ADD_VALUE_LABEL
#undef ADD_MENU_CONTAINER
#undef END_WINDOW

/****************************************************
 * Definitions below are for for file windows.usr
 */

/**
 * Create window, must be followed by max n ADD_WIDGET(...) and END_WINDOW()
 */
#define CREATE_WINDOW(name)                                                    \
  {                                                                            \
#name, {

/**
 * Adds label to window, window must be created before
 */
#define ADD_LABEL(name, x, y, width, height, visibility, textId, align)        \
  {#name, x, y, width, height, widget::type::LABEL, visibility, textId, align},

/**
 * Adds label to window, window must be created before
 */
#define ADD_VALUE_LABEL(name, x, y, width, height, visibility, source, align)  \
  {#name,      x,                                                              \
   y,          width,                                                          \
   height,     widget::type::LABEL,                                            \
   visibility, textId::_TXT_NO_TEXT,                                           \
   align,      source},

/**
 * Adds image to window, window must be created before
 */
#define ADD_IMAGE(name, x, y, width, height, visibility, imageId)              \
  {#name, x, y, width, height, widget::type::IMAGE, visibility, imageId},

/**
 * Adds icon to window, window must be created before
 */
#define ADD_BUTTON(name, x, y, width, height, visibility, textId, align)       \
  {#name,      x,      y,     width, height,        widget::type::BUTTON,      \
   visibility, textId, align, 0,     &handle_##name},

/**
 * Adds menu container to window,
 */
#define ADD_MENU_CONTAINER(name, x, y, width, height, visibility, textId,      \
                           align)                                              \
  {                                                                            \
#name, x, y, width, height, widget::type::MENU_CONTAINER, visibility,      \
        textId, align                                                          \
  }

/**
 * Closes window definition, must be added after all created widgets in window
 */
#define END_WINDOW()                                                           \
  }                                                                            \
  }                                                                            \
  ,

void value_event(int value, char buff[20]);
extern ct_gui::window windows[];