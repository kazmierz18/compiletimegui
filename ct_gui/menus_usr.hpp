#pragma once
#include "menu.hpp"
#include "translations.hpp"

#define MENUS_USR_FILE "../usr_gen/menus.usr"

/**
 *
 */
#define CREATE_MENU(name) name,
#define ADD_MENU_ITEM(name, textId)
#define ADD_SUBMENU(name, subMenuId, textId)
#define END_MENU()

enum menuId {
#include MENUS_USR_FILE
  _INVALID_MENU,
};

#undef CREATE_MENU
#undef ADD_MENU_ITEM
#undef ADD_SUBMENU
#undef END_MENU

/**
 *
 */
#define CREATE_MENU(name) _##name = -1,
#define ADD_MENU_ITEM(name, textId) name,
#define ADD_SUBMENU(name, subMenuId, textId) name,
#define END_MENU()

enum menuItemsId {
#include MENUS_USR_FILE
};

#undef CREATE_MENU
#undef ADD_MENU_ITEM
#undef ADD_SUBMENU
#undef END_MENU

/****************************************************
 * Definitions below are for for file menus.usr
 */

/**
 * Adds submenu to the menu
 * submenu is an menu_item, which will subsitute current menu in menu container
 * on click
 */
#define CREATE_MENU(name)                                                      \
  {                                                                            \
#name, {

/**
 * Adds menu item to the menu
 */
#define ADD_MENU_ITEM(name, textId) {#name, menuId::_INVALID_MENU, textId},

/**
 * Adds menu item to the menu which is link to submenu
 */
#define ADD_SUBMENU(name, subMenuId, textId) {#name, subMenuId, textId},

/**
 * Closes menu definition, must be added after al the menu items were added to
 * the menu
 */
#define END_MENU()                                                             \
  }                                                                            \
  }                                                                            \
  ,

extern ct_gui::menu menus[];