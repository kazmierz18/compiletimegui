#include "translations.hpp"
#include <cstdint>

namespace translate {

#define TRANSLATE(name, ang, ger, spa, pl, ru) {ang, ger, spa, pl, ru},
/**
 * C array containing all translations
 */
const char *texts[][static_cast<std::size_t>(language::_NUM)] = {
#include TRANSLATION_FILE
};
#undef TRANSLATE

const char *getTranslation(const textId id, const language l) {
  return texts[(int)id][(int)l];
}

/**
 * This variable keeps global language in system
 */
language lang = language::ENG;

void setLanguage(const language l) { lang = l; }

language getLanguage() { return lang; }

} // namespace translate