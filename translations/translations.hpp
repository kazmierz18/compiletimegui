#pragma once

namespace translate {
#define TRANSLATION_FILE "../usr_gen/translation.usr"
/**
 * Ids of texts from translations
 */
enum textId {
#define TRANSLATE(name, ang, ger, spa, pl, ru) TXT_##name,
#include TRANSLATION_FILE
  _TXT_LAST,
  _TXT_NO_TEXT, // this is for dynamic generation of texts
};
#undef TRANSLATE

/**
 * Possible languages
 */
enum class language {
  ENG = 0,
  GER,
  SPA,
  PL,
  RU,
  _NUM, // this must be last
};

/**
 * Get current text by ID
 */
const char *getTranslation(const textId id, const language l);

/**
 * Set language of system
 */
void setLanguage(const language l);

/**
 * Get current language of system
 */
language getLanguage();

} // namespace translate