#include "driver.hpp"
#include <GL/glut.h>
#include <cstring>
namespace driver {

const double colors[][3] = {{0.8, 0.7, 0.5}, {0.5, 0.7, 0.5}};

void driver_init() {}

void draw_rect(const int x, const int y, const int width, const int height,
               const int color) {
  glColor3f(colors[color][0], colors[color][1], colors[color][2]);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  gluOrtho2D(0, windows_width, windows_height, 0);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();

  glRasterPos2i(x, y);

  glRecti(x, y, x + width, y + height);

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
}

void draw_text(const char *text, const int x, const int y) {
  int len;
  len = strlen(text);

  glColor3f(1, 1, 1);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  gluOrtho2D(0, windows_width, windows_height, 0);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();

  glRasterPos2i(x, y);

  for (int i = 0; i < len; ++i) {
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, text[i]);
  }

  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
}

void redraw() { glutPostRedisplay(); }

} // namespace driver