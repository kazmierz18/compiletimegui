#pragma once

namespace driver {

const int windows_width = 500;
const int windows_height = 500;

/**
 * Initialize low level driver for drawing and handling event for GUI
 */
void driver_init();

/**
 * Starts infinite loop
 */
void driver_start();

void draw_rect(const int x, const int y, const int width, const int height,
               const int color);
void draw_text(const char *text, const int x, const int y);
void redraw();
} // namespace driver