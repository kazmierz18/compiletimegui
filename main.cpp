#include "driver.hpp"
#include "renderer.hpp"
#include "widget.hpp"
#include <ctime>
#include <random>

int main(int argc, char **argv) {
  srand(time(NULL));
  driver::driver_init();
  extern void usr_value_event(int, char[20]);
  ct_gui::registerValueGet(usr_value_event);
  ct_gui::renderer_init(windowId::WINDOW_1);
  ct_gui::renderer_start();
}
